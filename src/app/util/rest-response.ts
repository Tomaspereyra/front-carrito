export interface RestResponse {
    cod:number;
    mensaje:string;
    data:Map<String,any>;
}
