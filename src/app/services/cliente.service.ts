import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { LoginDto } from '../tienda/dto/login-dto';
import { ClienteDto } from '../tienda/dto/cliente-dto';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private httpCliente: HttpClient,private rest:RestService) { }

  iniciarSesion(form:LoginDto){
    let header = new HttpHeaders();
      header = header.set('Authorization', 'Basic YW5ndWxhcmFwcDpzZWNyZXRwYXNz');
      header = header.set('Content-Type','application/x-www-form-urlencoded');
  
      const payload = new HttpParams()
       .set('username', form.username)
        .set('password', form.password)
        .set('grant_type',form.grant_type);

    return this.httpCliente.post<any>("http://localhost:9100/oauth/token",payload,{headers:header});
  }

  traerClientePorEmail(email:string){
   
    let params:Map<string,string> = new Map();
    params.set("mail",email);
  
    return this.rest.sendGetRequest("http://localhost:8080/cliente/consulta",params);

  }
}
