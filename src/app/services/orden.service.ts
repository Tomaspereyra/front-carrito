import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { OrdenDto } from '../tienda/dto/orden-dto';
import { OrdenProductoDto } from '../tienda/dto/orden-producto-dto';

@Injectable({
  providedIn: 'root'
})
export class OrdenService {

  constructor(private rest:RestService) { }

  agregarProdutosAlCarrito(id:string, ordenDto:OrdenDto){
     
   return  this.rest.sendPutRequest("http://localhost:8080/orden/agregarproducto/"+id,ordenDto);
     

  }
  crearCarrito(ordenDto:OrdenProductoDto){
    return  this.rest.sendPostRequest("http://localhost:8080/orden/generar", ordenDto);

  }

  actualizarOrden(id:string,ordenDto:OrdenDto){
    return  this.rest.sendPutRequest("http://localhost:8080/orden/actualizar/"+id,ordenDto);
  }

  eliminarProductoDelCarrito(id:any){
    return this.rest.sendDeleteRequest("http://localhost:8080/orden/eliminarproducto/"+id);
  }



}
