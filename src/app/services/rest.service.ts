import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RestResponse } from '../util/rest-response';
@Injectable({
  providedIn: 'root'
})
export class RestService {

  
  constructor(private httpCliente: HttpClient) {

  }

 sendPostRequest(url:string,body:any,headers?: Map<string,string>){ 
         
  return this.httpCliente.post<RestResponse>(url,body);
 }
 sendGetRequest(url:string,params?:Map<string,string>,headers?:Map<string,string>){
   if(params){
    let param = new HttpParams();
    params.forEach((value,key)=>
    param=param.set(key,value));
    return this.httpCliente.get<RestResponse>(url,{params:param});
   }
  

  
   return this.httpCliente.get<RestResponse>(url);
 }

 sendPutRequest(url:string,body:any){
   return this.httpCliente.put<RestResponse>(url,body);

 }
 sendDeleteRequest(url:string){
   return this.httpCliente.delete<RestResponse>(url);
 }
}
