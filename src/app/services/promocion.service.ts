import { Injectable } from '@angular/core';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class PromocionService {

  constructor(private rest:RestService) { }

  public consultarPromocionViegente(){
    return this.rest.sendGetRequest("http://localhost:8080/promocion/consulta");
  }
}
