import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { SubFlujoTiendaComponent } from './sub-flujo-tienda/sub-flujo-tienda.component';
import {  NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TiendaRoutingModule } from './tienda-routing.module';
import { ProductoComponent } from './components/producto/producto.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { ItemComponent } from './components/item/item.component';
import { DetalleItemComponent } from './components/detalle-item/detalle-item.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';



@NgModule({
  declarations: [HeaderComponent, SubFlujoTiendaComponent, ProductoComponent, CarritoComponent, ItemComponent, DetalleItemComponent, SpinnerComponent, LoginComponent],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    TiendaRoutingModule
  ]
})
export class TiendaModule { }
