import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubFlujoTiendaComponent } from './sub-flujo-tienda.component';

describe('SubFlujoTiendaComponent', () => {
  let component: SubFlujoTiendaComponent;
  let fixture: ComponentFixture<SubFlujoTiendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubFlujoTiendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubFlujoTiendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
