import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

export class LoginDto {
    public username:string;
    public password:string;
    public grant_type:string;
}
