export class ClienteDto {
    nombre: string;
    apellido: string;
    email: string;
    password: string;
    dni: number;
    tipoCliente: any;
}
