import { OrdenProductoDto } from './orden-producto-dto';

export class OrdenDto {
    public id:Number;
    public cliente:any;
    public productos:Array<any>;
    public total:number;
    public fecha:number;
    public finalizada:boolean;


    constructor(){

    }

}
