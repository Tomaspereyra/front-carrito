import { Component, OnInit } from '@angular/core';
import { LoginDto } from '../../dto/login-dto';
import { ClienteService } from 'src/app/services/cliente.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginDto:LoginDto = new LoginDto();
  cargando:Boolean = false;
  error:Boolean = false;
  type:string = "warning";
  constructor(private clienteService:ClienteService,private modalService:NgbModal) { }

  ngOnInit(): void {
  }
  onSubmit(){
    this.cargando = true;
    this.loginDto.grant_type ="password";
    this.clienteService.iniciarSesion(this.loginDto).subscribe(res=>{
       
      if(res.access_token){
          localStorage.setItem("token",res.access_token);
          this.modalService.dismissAll();
        }
      else{
        this.error = true;

      }
    },err=>{console.log(err);
      this.cargando = false;
      this.error = true; })
  }
  

}
