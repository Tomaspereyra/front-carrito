import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() nombre:String;
  @Input() descripcion:String;
  @Input() categoria:String;
  @Input() url:String;

  @Output() verMas= new EventEmitter<Boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  enviarEvento(verDetalle:boolean){
    this.verMas.emit(verDetalle);
  }



}
