import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';
import { OrdenDto } from '../../dto/orden-dto';
import { OrdenService } from 'src/app/services/orden.service';
import { PromocionService } from 'src/app/services/promocion.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import { OrdenProductoDto } from '../../dto/orden-producto-dto';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {
  productos:any = []
  ordenDto:OrdenDto = new OrdenDto();
   cargando:boolean = false;
   compraFinalizada:number = 0;
   idCarrito:string;
   subTotal:number = 0;
   total:number  = 0;
   descuento:number = 0;
   promocion:boolean = false;
   usuarioVip:boolean =false;
   logeado:boolean=false;

  constructor(private rest:RestService,private modalService:NgbModal,private ordenService:OrdenService,private promocionService:PromocionService,private clienteService:ClienteService) { }

  ngOnInit(): void {
    this.consultarPromocionVigente()
    this.idCarrito = localStorage.getItem("idOrden");
    this.cargarProductos();
    this.traerUsuario();  
    this.logeado = this.estaLogeado();
        
    
  }
  login(){
  const modal= this.modalService.open(LoginComponent);
  modal.result.then(onClose=>{
   
  },onDismiss=>{
    //se ejecutara este bloque si el usuario se loguea
  
    this.traerUsuario();
  }
  )
     
  }

  cargarProductos(){
   

    this.rest.sendGetRequest("http://localhost:8080/orden/"+this.idCarrito).subscribe(res=>{
      if(res.cod == 200){
        let ordenRes:any = res.data;
        console.log(ordenRes);
        
        this.productos = ordenRes.orden.productos;
        this.ordenDto = ordenRes.orden;
      
        this.calcular();
      }
    })
  }
  calcular(){
    this.subTotal =  this.calcularSubTotal();
    this.descuento= this.calcularDescuento();
     
    this.total= this.subTotal - this.descuento;

  }
  calcularSubTotal():number{
    let subTotal = 0;
    for(let item of this.productos){     
      subTotal= subTotal+(item.producto.precio*item.cantidad);


    }
    return subTotal;
  }
  traerEmailCliente(){
    let token =localStorage.getItem("token");
    let email:string;
    if(token!=null){
      email = JSON.parse(atob(token.split(".")[1])).user_name;
  }
  return email;
  }
  calcularDescuento():number{
    let cantProductos = this.calcularCantidad();
    
    let descuento = 0;
      if(cantProductos == 5){
        descuento = (this.subTotal * 20 ) / 100;

       }else if(cantProductos >10){
          if(this.usuarioVip)
            descuento = 700;
          else if(this.promocion)
                   descuento = 500;
          
          
       }
       return descuento;

    }
    calcularCantidad():number{
      let cantidad: number = 0;
      for(let item of this.productos){
          cantidad += item.cantidad;
      }
      return cantidad;
    }

    consultarPromocionVigente(){

      this.promocionService.consultarPromocionViegente().subscribe(res=>{
        if(res.cod==200){
          this.promocion=true;
        }else{
          this.promocion=false;
        }
      })
    }



  
  eliminarProducto(index:any,id:any){
   this.productos.splice(index, 1);
   console.log(this.ordenDto,this.productos);
   this.ordenDto.productos = this.productos;
   
   this.ordenService.eliminarProductoDelCarrito(id).subscribe(res=>{
     if(res){
       console.log("eliminado con exito");
       this.calcular();
     }
   },err=>(console.log("error",err)));
   
  }

  finalizarCompra(){
    if(!localStorage.getItem("token")){

      this.login();
    }else{
    this.ordenDto.finalizada = true;
    this.ordenDto.total=this.total;
    this.ordenService.actualizarOrden(this.idCarrito,this.ordenDto).subscribe(res=>{
      if(res.cod==200){
        localStorage.removeItem("idOrden")
        console.log("Orden finalizada con exito");
        this.compraFinalizada = 1;
        
      }else if(res.cod == 201){
        
        localStorage.removeItem("idOrden")
        this.compraFinalizada = 2;
       
      }
    },err=>(console.log("error",err)));


  }
}

traerUsuario(){
  if(localStorage.getItem("token")){
  this.clienteService.traerClientePorEmail(this.traerEmailCliente()).subscribe(res=>{
    if(res.cod == 200){
      let response:any = res.data;
      this.ordenDto.cliente = response.cliente;
      console.log("###########",this.ordenDto.cliente.tipoCliente.nombre);
      if(this.ordenDto.cliente.tipoCliente.nombre == "VIP"){
        this.usuarioVip = true;
        this.calcular();
      }
    }
  },err=>(console.log(err)))
}
}
estaLogeado():boolean{
  if(localStorage.getItem("token")){
    return true;
  }else{
    return false;
  }
}
salir(){
  localStorage.removeItem("token");
  this.logeado= false;
}
}
