import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {
  imagenes = ["assets/nike97.jpeg","assets/images.jpeg","assets/pantalon.jpg"];
  verDetalle:boolean;
  listaproducto:any;
  productoSeleccionado:any;
  logeado:boolean=false;
  @Output() irA = new EventEmitter<String>();
  constructor(private restService:RestService) { }

  ngOnInit(): void {
    this.verDetalle = false;
    this.traerProductos();
    this.logeado = this.estaLogeado();
  }
  estaLogeado():boolean{
    if(localStorage.getItem("token")){
      return true;
    }else{
      return false;
    }
  }
  
  mostrarDetalle(producto:any){
    this.verDetalle = true;
    this.productoSeleccionado=producto;
    console.log(this.productoSeleccionado);
  }
  traerProductos(){
    this.restService.sendGetRequest("http://localhost:8080/productos/destacados").subscribe(res=>{
      if(res.cod == 200){
        let data:any;
        data = res.data
        
        this.listaproducto = data.productos;
        console.log(this.listaproducto);
      }
      
    },err=>(console.log("error")));

  }
  procesarEvento(event:string){
    switch(event){
      case "MiCarrito":
        this.verDetalle=true;
        this.irA.emit("MiCarrito");
        console.log("mi carrito");
        break;

    }

  }
  salir(){
    localStorage.removeItem("token");
    this.logeado= false;
  }

}
