import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';
import { OrdenDto } from '../../dto/orden-dto';
import { OrdenService } from 'src/app/services/orden.service';
import { OrdenProductoDto } from '../../dto/orden-producto-dto';

@Component({
  selector: 'app-detalle-item',
  templateUrl: './detalle-item.component.html',
  styleUrls: ['./detalle-item.component.scss']
})
export class DetalleItemComponent implements OnInit {
  images = ["/assets/nike97.jpeg", "/assets/nike97.jpeg", "/assets/nike97.jpeg"]
  @Input() producto:any;
  @Output() irA = new EventEmitter<String>()
  cargando:boolean=false;
  cantidad:number;

 
  
  ordenDto:OrdenDto = new OrdenDto();
  ordenProductoDto:OrdenProductoDto = new OrdenProductoDto();


  constructor(private ordenService:OrdenService) { }

  ngOnInit(): void {
    console.log(this.producto);
    
  }

   agregarAlCarrito(){
    this.cargando = true;
    this.ordenProductoDto.producto = this.producto;
     let id:string=localStorage.getItem('idOrden') ; 
    if(id == null){
      this.generarCarrito();

    }else{
      let productos = new Array<OrdenProductoDto>();
      productos.push(this.ordenProductoDto);
      this.ordenDto.productos= productos; 

      
      this.agregarProductoAlCarrito(id);
    }
    
  
  }
  
  
  showModal(){
    this.generarCarrito();

  }

  

   generarCarrito(){
    

      this.ordenService.crearCarrito(this.ordenProductoDto).subscribe(res=>{
     if (res.cod == 200) {
        let id:any = res.data;
        localStorage.setItem("idOrden",id.ordenId);
        this.cargando = false;
        this.irA.emit("MiCarrito");
      }
      else {
        console.log(res.mensaje);
      }
    });
   }

   agregarProductoAlCarrito(id:string){
     
     this.ordenService.agregarProdutosAlCarrito(id,this.ordenDto).subscribe(res=>{
      if (res.cod == 200) {
        this.cargando = false;
        this.irA.emit("MiCarrito");
      }
      else {
        console.log(res.mensaje);
      }
     })

   }
}
