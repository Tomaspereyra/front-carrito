import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubFlujoTiendaComponent } from './sub-flujo-tienda/sub-flujo-tienda.component';
import { DetalleItemComponent } from './components/detalle-item/detalle-item.component';


const routes: Routes = [
  {path:'',component:SubFlujoTiendaComponent},

   {path:'detalle',component:DetalleItemComponent, outlet:"tienda"}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TiendaRoutingModule { }
